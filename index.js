
// Exemple de composant fonctionnel
function Bienvenue({name, children}) {
    return (
        <div>
            <h1>Bonjour {name}</h1>
            <p>
                {children}
            </p>
        </div>
    )
}

ReactDOM.render(
    <Bienvenue name="Jean">Bonjour tout le monde</Bienvenue>,
    document.querySelector('#app')
)

// exemple de composant aves une classe

class Salut extends React.Component{

    render (){
        return (
            <div>
                <h1>salut {this.props.name}</h1>
                <p>
                    {this.props.children}
                </p>
            </div>
        )
    }

}

ReactDOM.render(
    <Bienvenue name="Michel">Bonjour tout le monde</Bienvenue>,
    document.querySelector('#app1')
)
