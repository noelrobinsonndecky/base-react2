// Les props en React
// exemple 1: 
function MaFonction(props){
    return (
        <h1>Bonjour c'est {props.name}</h1>
    )
}

ReactDOM.render(
    <MaFonction name="Noel"/>,
    document.querySelector('#app')
)

// exmple 2: avec destructuration de propriétés
function MaFonction1({name}){
    return (
        <h2>Bonjour M. {name}</h2>
    )
}

ReactDOM.render(
    <MaFonction1 name="Christ"/>,
    document.querySelector('#app1')
)

// exemple 3: avec la propriété children
function MaFonction2({name, children}){
    return (
        <div>
            <h3>Salut Mdme {name}</h3>
            <p>
                {children}
            </p>
        </div>
    )
}

ReactDOM.render(
    <MaFonction2 name="Fati">Bonjour tout le monde</MaFonction2>,
    document.querySelector('#app2')
)

//Exemple d'un composant sous la forme d'une classe
// exemple 1:
class Bienvenue extends React.Component{

    render () {
        return (
            <div>
                <h1>Salut Noel et {this.props.name}</h1>
                <p>
                    {this.props.children}
                </p>
            </div>
        )
    }

}

ReactDOM.render(
    <Bienvenue name="Gomez">Bonjour à vous autres vous allez bien ?</Bienvenue>,
    document.querySelector('#app3')
)

// exemple 2: avec un constructeur en utilisant la methode super()

class Bienvenue1 extends React.Component{

    constructor (props){
        super(props)
        console.log(props);
    }

    render () {
        return (
            <div>
                <h1>Bonsoir les gars et {this.props.name}</h1>
                <p>
                    {this.props.children}
                </p>
            </div>
        )
    }
}

ReactDOM.render(
    <Bienvenue1 name="la journée">J'espère que vous allez bien ? on fait quoi aujourdh'ui ?</Bienvenue1>,
    document.querySelector('#app4')
)


class Clock extends React.Component{

    render(){
        const date = new Date()
        return (
            <div>
                il est: {date.toLocaleDateString()} {date.toLocaleTimeString()}
            </div>
        )
    }
}


// Utiliser un composant déja crée à l'intérieur d'un autre composant
//exemple 1:
function Accueil(){
    return (
        <div>
            <MaFonction1 name="Ali"/>,
            <MaFonction1 name="Robert"/>,
            <Clock/>
        </div>
    )
}

ReactDOM.render(
    <Accueil/>,
    document.querySelector('#app5')
)
