// La notion d'état et des cycles de vies des composants

// Mon composant fonctionnel 

function MaFonction1({name}){
    return (
        <h2>Bonjour M. {name}</h2>
    )
}

// Mon composant clock

class Clock extends React.Component{
    // Les Etats 
    /**
     * Pour utiliser un Etat il faut un constructeur: constructor(){}
     * L'Etat de notre composant c'est un objet qui va représenté les données qui sont utiles
     * à l'intérieur du composant et qui ne seront pas exposées à des propriétés.
     * L'Etat c'est un objet qui est spécifique et accessible directement grace à une propriété nommée: State.
     */

    constructor(props){
        super(props)
        this.state= {date: new Date()}
        this.timer= null
    }
    
    // Cycles de vie des composants

    //componentDidMount: permet de déterminer quand est-ce qu'un composant a été monté;
    componentDidMount(){
        this.timer= window.setInterval(this.tick.bind(this),1000)
    }

    //componentWillUnmount: permet de déterminer quand est-ce qu'un composant a été démonté;
    componentWillUnmount(){
        window.clearInterval(this.timer)
    }

    // Ma fonction qui permettra de changer l'Etat
    tick (){
        // Pour changer l'Etat d'un composant il faut utiliser la méthode: setState()
        this.setState({date: new Date()})
    }

    render() {
        const date= new Date();
    return (
        <div>
            il est: {this.state.date.toLocaleDateString()} {this.state.date.toLocaleTimeString()}
        </div>
    )
    }
}


// Exemple d'un composant d'incrémentation

class Incrementeur extends React.Component{

    // Mon constructeur
    constructor(props){
        super(props)
        this.state = {n: props.start}
        this.timer = null
    }

    // Mon componentDidMount
    componentDidMount(){
       window.setInterval(this.incrementer.bind(this),1000)
    }

    // Mon componentWillUnmont
    componentWillUnmount(){
        window.clearInterval(this.timer)
    }

    // Ma fonction incrementer
    incrementer(){
        this.setState({n: this.state.n + 1})
    }

    render () {
        return (
            <div>
                {this.state.n}
                <p>
                    {this.props.children}
                </p>
            </div>
        )
    }
}


// Mon composant principal

class ComposantPrincipal extends React.Component{

    render () {
        return (
            <div>

                <div id="A">
                    <div class="aa">
                        <MaFonction1 name="Christ" />
                    </div>
                    <div class="aa">
                        <MaFonction1 name="Noel" />
                    </div>
                    <div class="aa">
                        <MaFonction1 name="Gueye" />
                    </div>             
                </div>
                
                <div id="B">
                    <Clock />
                </div>
                
                <div id="C">
                    <div class="Cc">
                        <div class="cc">
                            <h2>Compteur 0:<Incrementeur start={0}/></h2>
                        </div>
                        <div class="cc">
                            <h2>Compteur 1: <Incrementeur start={10}/></h2>
                        </div>
                        <div class="cc">
                            <h2>Compteur 2: <Incrementeur start={20}/></h2>
                        </div>
                        <div class="cc">
                            <h2>Compteur 3: <Incrementeur start={30}/></h2>
                        </div>
                        <div class="cc">
                            <h2>Compteur 4: <Incrementeur start={40}/></h2>
                        </div>
                    </div>
                    
                    <div class="Cc">
                        <div class="cc">
                            <h2>Compteur 5: <Incrementeur start={50}/></h2>
                        </div>
                        <div class="cc">
                            <h2>Compteur 6: <Incrementeur start={60}/></h2>
                        </div>
                        <div class="cc">
                            <h2>Compteur 7: <Incrementeur start={70}/></h2>
                        </div>
                        <div class="cc">
                            <h2>Compteur 8: <Incrementeur start={80}/></h2>
                        </div>
                        <div class="cc">
                            <h2>Compteur 90: <Incrementeur start={90}/></h2>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

ReactDOM.render(
    <ComposantPrincipal/>,
    document.querySelector('#app')
)